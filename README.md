# linux_downloader
Automatically download the latest version of some of the most common linux 
distributions. It will automatically download either the iso or the torrent file
for all supported linux distributions. If it already exists it will skip over 
it, if it is partially downloaded it will continue the download. If the download 
is an iso, it will validate the file with a checksum, if it fails it will remove
the downloaded file and reinitiate the download. It is recommened to download a 
torrent file and output it to a watch folder for a torrent client.

# Usage
```bash
$ ./linux_downloader.sh [options]
```

# Options
```bash
-e             Change the type of files to download [iso,torrent], Default is torrent 
-o             Change the output directory of the downloaded files, Default is current directory of terminal
--archlinux    Download Archlinux, will not download others unless selected
--centos       Download CentOS,    will not download others unless selected
--fedora       Download Fedora,    will not download others unless selected
--kali         Download Kali,      will not download others unless selected
--ubuntu       Download Ubuntu,    will not download others unless selected
```

# Supported File Types
- Iso
- Torrent

# Supported Distros
- Archlinux
- Centos
- Fedora
- Kali
- Ubuntu