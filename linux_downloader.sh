#/bin/bash

# Source: http://mywiki.wooledge.org/BashFAQ/035
die() {
    printf '%s\n' "$1" >&2
    exit 1
}

extension=".torrent"
flags="-r -np -nd -c -q --show-progress -e robots=off"
output="."

# Download list
all=1
archlinux=0
Centos=0
Fedora=0
Kali=0
Ubuntu=0

# Source: http://mywiki.wooledge.org/BashFAQ/035
while :; do
  case $1 in
    -h|-\?|--help)
      echo "Automatically download latest linux distributions"
      echo "Usage:"
      echo "    ./linux_downloader.sh [options]"
      echo ""
      echo "General Options:"
      echo "    -h, --help               Display this help message"
      echo "    -e, --extension          Change the download type to either [iso,torrent], Default is torrent"
      echo "    -o, --output             Change the download directory, Default is current directory"
      echo "    --archlinux              Download Archlinux, will not download others unless selected"
      echo "    --centos                 Download CentOS,    will not download others unless selected"
      echo "    --fedora                 Download Fedora,    will not download others unless selected"
      echo "    --kali                   Download Kali,      will not download others unless selected"
      echo "    --ubuntu                 Download Ubuntu,    will not download others unless selected"
      echo ""
      echo "Example:"
      echo "    ./linux_downloader.sh -e iso -o ~/Downloads/ --archlinux --ubuntu --fedora"
      echo ""
      exit 0
      ;;
    -e|--extension)       # Takes an option argument; ensure it has been specified.
      if [ "$2" ]; then
        extension=.$2
        shift
      else
        die 'ERROR: "--extension" requires a non-empty option argument.'
      fi
      ;;
    -o|--output)       # Takes an option argument; ensure it has been specified.
      if [ "$2" ]; then
        output=$2
        mkdir -p $output
        shift
      else
        die 'ERROR: "--extension" requires a non-empty option argument.'
      fi
      ;;
    --archlinux)
      all=0
      archlinux=1
      ;;  
    --centos)
      all=0
      centos=1
      ;;
    --fedora)
      all=0
      fedora=1
      ;;
    --kali)
      all=0
      kali=1
      ;;
    --ubuntu)
      all=0
      ubuntu=1
      ;;
    --)              # End of all options.
      shift
      break
      ;;
    -?*)
      printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
      ;;
    *)               # Default case: No more options, so break out of the loop.
      break
  esac

  shift
done

# Archlinux
if [[ $all -eq 1 || $archlinux -eq 1 ]]; then
  echo "Downloading Archlinux"
  arch=ftp://mirror.rackspace.com/archlinux/iso
  release=$(curl -l --silent $arch/ | sort -n -r | awk NR==1)

  if [ $extension == ".torrent" ]; then
    wget $flags $arch/$release/*$extension -P $output
  else
    # Archlinux
    while true; do
      wget $flags $arch/$release/*$extension -P $output
      wget $flags $arch/$release/sha1sums.txt -P $output

      filename=$(cat $output/sha1sums.txt | grep .iso  | cut -d' ' -f3)

      checksum=$(cat $output/sha1sums.txt | grep .iso | cut -d' ' -f1)
      filechecksum=$(sha1sum $output/$filename | cut -d' ' -f1) 

      if [ $checksum = $filechecksum ]; then
        break
      fi

      echo "$filename failed, Redownloading"
      rm $output/$filename
    done

    rm $output/sha1sums.txt
  fi
  echo "Archlinux Done"
fi

# CentOS
if [[ $all -eq 1 || $centos -eq 1 ]]; then
  echo "Downloading Centos"
  centos=ftp://mirror.rackspace.com/CentOS
  release=$(curl -l --silent $centos/ | sort -n -r | awk NR==1)

  if [ $extension == ".torrent" ]; then
    wget $flags $centos/$release/isos/x86_64/*{Everything,DVD}*$extension -P $output
  else
    # CentOS Everything
    while true; do
      type="Everything"
      wget $flags $centos/$release/isos/x86_64/*$type*$extension -P $output
      wget $flags $centos/$release/isos/x86_64/sha256sum.txt -P $output

      filename=$(cat $output/sha256sum.txt | grep $type | cut -d' ' -f3)

      checksum=$(cat $output/sha256sum.txt | grep $type | cut -d' ' -f1)
      filechecksum=$(sha256sum $output/$filename | cut -d' ' -f1) 

      if [ $checksum = $filechecksum ]; then
        break
      fi

      echo "$filename failed, Redownloading"
      rm $output/$filename
    done

    # CentOS DVD
    while true; do
      type="DVD"
      wget $flags $centos/$release/isos/x86_64/*$type*$extension -P $output
      wget $flags $centos/$release/isos/x86_64/sha256sum.txt -P $output

      filename=$(cat $output/sha256sum.txt | grep $type | cut -d' ' -f3)

      checksum=$(cat $output/sha256sum.txt | grep $type | cut -d' ' -f1)
      filechecksum=$(sha256sum $output/$filename | cut -d' ' -f1) 

      if [ $checksum = $filechecksum ]; then
        break
      fi

      echo "$filename failed, Redownloading"
      rm $output/$filename
    done

    rm $output/sha256sum.txt

  fi
  echo "Centos Done"
fi

# Fedora
if [[ $all -eq 1 || $fedora -eq 1 ]]; then 
  echo "Downloading Fedora"
  if [ $extension == ".torrent" ]; then
    fedora=https://torrent.fedoraproject.org/torrents
    workstation64=$(curl -l --silent $fedora/ | grep 'Fedora-Workstation-' | grep 'x86_64' | awk -F 'href="' '{print $2}' | cut -d'"' -f1 | sort -n -r | awk NR==1)
    workstation32=$(curl -l --silent $fedora/ | grep 'Fedora-Workstation-' | grep 'i386' | awk -F 'href="' '{print $2}' | cut -d'"' -f1 | sort -n -r | awk NR==1)
    server64=$(curl -l --silent $fedora/ | grep 'Fedora-Server-' | grep 'x86_64' | awk -F 'href="' '{print $2}' | cut -d'"' -f1 | sort -n -r | awk NR==1)
    server32=$(curl -l --silent $fedora/ | grep 'Fedora-Server-' | grep 'i386' | awk -F 'href="' '{print $2}' | cut -d'"' -f1 | sort -n -r | awk NR==1)
    
    wget $flags -A "*$extension" $fedora/{$workstation32,$workstation64,$server64,$server32} -P $output
  else
    fedora=$(curl -Ls -o /dev/null -w %{url_effective} https://download.fedoraproject.org)
    fedora="${fedora}releases"
    release=$(curl -l --silent $fedora/ | awk -F 'href="' '{print $2}' | cut -d'/' -f1 | sort -n -r | awk NR==1)
    fedora="$fedora/$release"
    
    # Fedora Everything
    while true; do
      type="Everything"
      
      wget $flags -R "*manifest" -A "Fedora*$extension" $fedora/$type/x86_64/iso/ -P $output
      wget $flags -A "*CHECKSUM" $fedora/$type/x86_64/iso/ -P $output
      
      filename=$(cat $output/Fedora*$type*$release*x86_64*CHECKSUM | awk -F '# ' '{print $2}' | cut -d':' -f1)
      filename=$(echo $filename | cut -d' ' -f2)
      
      checksum=$(cat $output/Fedora*$type*$release*x86_64*CHECKSUM | awk -F 'iso) =' '{print $2}')
      filechecksum=$(sha256sum $output/$filename | cut -d' ' -f1)

      if [ $checksum = $filechecksum ]; then
        break
      fi

      echo "$filename failed, Redownloading"
      rm $output/$filename
    done

    # Fedora Workstation
    while true; do
      type="Workstation"
      
      wget $flags -R "*manifest" -A "Fedora*$extension" $fedora/$type/x86_64/iso/ -P $output
      wget $flags -A "*CHECKSUM" $fedora/$type/x86_64/iso/ -P $output
      
      filename=$(cat $output/Fedora*$type*$release*x86_64*CHECKSUM | awk -F '# ' '{print $2}' | cut -d':' -f1)
      filename=$(echo $filename | cut -d' ' -f2)
      
      checksum=$(cat $output/Fedora*$type*$release*x86_64*CHECKSUM | awk -F 'iso) =' '{print $2}')
      filechecksum=$(sha256sum $output/$filename | cut -d' ' -f1)

      if [ $checksum = $filechecksum ]; then
        break
      fi

      echo "$filename failed, Redownloading"
      rm $output/$filename
    done

    # Fedora Server
    while true; do
      type="Server"
      
      wget $flags -R "*manifest" -A "Fedora*$extension" $fedora/$type/x86_64/iso/ -P $output
      wget $flags -A "*CHECKSUM" $fedora/$type/x86_64/iso/ -P $output
      
      filename=$(cat $output/Fedora*$type*$release*x86_64*CHECKSUM | awk -F '# ' '{print $2}' | cut -d':' -f1)
      filename=$(echo $filename | cut -d' ' -f2)

      checksum=$(cat $output/Fedora*$type*$release*x86_64*CHECKSUM | awk -F 'iso) =' '{print $2}')
      filechecksum=$(sha256sum $output/$filename | cut -d' ' -f1)

      if [ $checksum = $filechecksum ]; then
        break
      fi

      echo "$filename failed, Redownloading"
      rm $output/$filename
    done

    rm $output/Fedora*$type*$release*x86_64*CHECKSUM
  fi
  echo "Fedora Done"
fi

# Kali
if [[ $all -eq 1 || $kali -eq 1 ]]; then
  echo "Downloading Kali"
  kali=https://cdimage.kali.org/kali-images/current
  filename=$(curl -l --silent $kali/ | awk -F 'href="' '{print $2}' | cut -d'"' -f1 | grep amd64 | sort -n | awk NR==1)

  if [ $extension == ".torrent" ]; then
    torrent=https://images.offensive-security.com
    wget $flags $torrent/${filename}${extension} -P $output
  else
    # Kali
    while true; do
      wget $flags -A "*$extension" $kali/ -P $output
      wget $flags $kali/SHA256SUMS -P $output

      checksum=$(cat $output/SHA256SUMS | grep $filename | cut -d' ' -f1)
      filechecksum=$(sha256sum $output/$filename | cut -d' ' -f1) 

      if [ $checksum = $filechecksum ]; then
        break
      fi

      echo "$filename failed, Redownloading"
      rm $output/$filename
    done

    rm $output/SHA256SUMS
  fi
  echo "Kali Done"
fi

# Ubuntu
if [[ $all -eq 1 || $ubuntu -eq 1 ]]; then
  echo "Downloading Ubuntu"
  ubuntu=ftp://releases.ubuntu.com/releases
  release=$(curl -l --silent $ubuntu/ | sort -n -r | awk NR==1)

  if [ $extension == ".torrent" ]; then
    wget $flags $ubuntu/$release/*$extension -P $output
  else
    # Ubuntu Desktop 
    while true; do
      type="desktop"
      wget $flags $ubuntu/$release/*$type*$extension -P $output
      wget $flags $ubuntu/$release/SHA256SUMS -P $output

      filename=$(cat $output/SHA256SUMS | grep $type | cut -d' ' -f2)

      checksum=$(cat $output/SHA256SUMS | grep $type | cut -d' ' -f1)
      filechecksum=$(sha256sum $output/$filename | cut -d' ' -f1) 

      if [ $checksum = $filechecksum ]; then
        break
      fi

      echo "$filename failed, Redownloading"
      rm $output/$filename
    done

    # Ubuntu Server
    while true; do
      type="server"
      wget $flags $ubuntu/$release/*$type*$extension -P $output
      wget $flags $ubuntu/$release/SHA256SUMS -P $output

      filename=$(cat $output/SHA256SUMS | grep $type | cut -d' ' -f2)

      checksum=$(cat $output/SHA256SUMS | grep $type | cut -d' ' -f1)
      filechecksum=$(sha256sum $output/$filename | cut -d' ' -f1) 

      if [ $checksum = $filechecksum ]; then
        break
      fi

      echo "$filename failed, Redownloading"
      rm $output/$filename
    done
  fi

  echo "Ubuntu Done"
fi